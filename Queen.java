// package chess;


public class Queen {

    
    public Queen (){
        this.col = 0;
        this.row = 0;
        calcAttackSquares();
    }
    public Queen (int r, int c){

        // char attacks[][] = new char[8][8];
        // int qCol=c, qRow = r;

        // for (int row=0; row < 8; row++) {
    
        //     for (int col=0; col<8; col++){
        //         attacks[row][col] = ' ';
        //         if ((row == qRow) ) attacks[row][col] = 'X';
        //         if (col == qCol) attacks[row][col] = 'X';
        //     }
            
        // }
        
        // for (int row=0; row < 8; row++) {
        //     if (((qCol + row ) < 8) && ((qRow + row ) < 8)) attacks[qRow+row][qCol+row] = 'X';
        //     if (((qCol - row ) >= 0) && ((qRow + row ) < 8)) attacks[qRow+row][qCol-row] = 'X';
        //     if (((qCol - row ) >= 0) && ((qRow - row ) >= 0))   attacks[qRow-row][qCol-row] = 'X';
        //     if (((qCol + row ) < 8) && ((qRow - row ) >= 0))   attacks[qRow-row][qCol+row] = 'X';
            
        // }
        
        // attacks[qRow][qCol] = 'D';

        // this.attacks = attacks;

        this.col = c;
        this.row = r;
        calcAttackSquares();

    }

    public void setPosition(int r, int c) {
        this.row = r;
        this.col = c;
        calcAttackSquares();
    }
    
    public void nextPosition() {
        int tempCol= this.col;
        int tempRow = this.row;

        tempCol++;
        if (tempCol > 7) {
            tempCol=0;
            tempRow++;
        }
        if (tempRow > 7){
            tempRow =0;
        }

        this.row = tempRow;
        this.col = tempCol;
        calcAttackSquares();
    }
    void calcAttackSquares(){
        char attacks[][] = new char[8][8];
        int qCol=this.col, qRow = this.row;

        for (int row=0; row < 8; row++) {
    
            for (int col=0; col<8; col++){
                attacks[row][col] = ' ';
                if ((row == qRow) ) attacks[row][col] = 'X';
                if (col == qCol) attacks[row][col] = 'X';
            }
            
        }
        
        for (int row=0; row < 8; row++) {
            if (((qCol + row ) < 8) && ((qRow + row ) < 8)) attacks[qRow+row][qCol+row] = 'X';
            if (((qCol - row ) >= 0) && ((qRow + row ) < 8)) attacks[qRow+row][qCol-row] = 'X';
            if (((qCol - row ) >= 0) && ((qRow - row ) >= 0))   attacks[qRow-row][qCol-row] = 'X';
            if (((qCol + row ) < 8) && ((qRow - row ) >= 0))   attacks[qRow-row][qCol+row] = 'X';
            
        }
        
        attacks[qRow][qCol] = 'D';

        this.attacks = attacks;

    }
    public int getCol(){
        return this.col;
    }
    public int getRow() {
        return this.row;
    }

    public void print(){
        char b[][] = attacks;
        char board[][] = new char[8][8];
            boolean whiteSquare = false;
            System.out.println("____________________________");
            for (int row=0; row < 8; row++) {
    
                for (int col=0; col<8; col++){
                    
                    if (whiteSquare) board[row][col] = '_';
                    else board[row][col] = '#';
                    whiteSquare = !whiteSquare;
                    if (b[row][col] != ' '){
                        board[row][col] = b[row][col];
                        //System.out.println("True");
                    }}
            }
            System.out.println(" ABCDEFGH");
            //board = b;
            for (int row=7; row >= 0; row--) {
                System.out.print(row+1);
                
                for (int col=0; col<8; col++){
                    System.out.print(board[row][col]);
                    
                }
                System.out.print(row+1);
                System.out.println("");
    
            }
            System.out.println(" ABCDEFGH");
            System.out.println("____________________________");
    
    }

    public int col, row;
    public char attacks[][];

}