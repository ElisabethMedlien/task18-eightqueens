// package chess;

// import chess.*;

import java.util.*;
import java.util.ArrayList;
import java.util.Random;

public class Task18 {

    // public class Queen
    static boolean[][] board;
    static final int N = 8;

    public static void main(String[] args) {
        
        ArrayList<Solution> solutions = new ArrayList<Solution>() ;
        int counter = 0;
        Queen queens[] = new Queen[8];
        for (int i=0;i<8;i++) queens[i] = new Queen();
        queens[0].setPosition(0, 0);
        for (int k=0;k<8;k++){
            for (int l=0;l<8;l++) {
                queens[k].setPosition(k, l); 
            }
        }
        int inputX=0, inputY=0;

        switch( args[0].toLowerCase().charAt(0)){
            case 'a': inputX = 0;
            break;
            case 'b': inputX = 1;
            break;
            case 'c': inputX = 2;
            break;
            case 'd': inputX = 3;
            break;
            case 'e': inputX = 4;
            break;
            case 'f': inputX = 5;
            break;
            case 'g': inputX = 6;
            break;
            case 'h': inputX = 7;
            break;
       }
       inputY = Integer.parseInt(args[1])-1;
        

        

        System.out.println("Counter: " + counter);
            Solution test = new Solution();
            test = getSolution(inputX, inputY);
            solutions.add(test);

            boolean duplicate = true;

            for (int n=0;n<0;n++){
            test = getSolution(inputX, inputY);
            duplicate = true;
            for (Solution s : solutions) {
                for (int i=0; i<8; i++) if (s.squares[i] != test.squares[i]) duplicate= false;
            }
            if (duplicate == false) solutions.add(test);
            else  System.out.println("Duplicate");
        }

        System.out.println("___________________________");
        System.out.println("Printing solutions:");
        for (Solution s : solutions) {
            s.print();
        }
    }

    public static Solution getSolution(int x, int y) {
        boolean conflict = false;
        long counter =0;
        Queen queens[] = new Queen[8];
        for (int i=0;i<8;i++) queens[i] = new Queen();
        for (int k=0;k<8;k++){
            for (int l=0;l<8;l++) {
                queens[k].setPosition(k, l); 
            }
            // queens[k].print();
        }
        queens[0].setPosition(y,x);
        System.out.println("Row: " +y+" Col: " +x);
        for (int i=1;i<8;i++){
            conflict = false;
            // if (i==1)System.out.println("Given array pos " + 0 + " at " + queens[0].row + ", " + queens[0].col);
            
            while (true){
                counter++;
                conflict = false;
                for (int row=0; row <= i; row++) {
                    if ((counter > 50)) {
                        // if (i>6)System.out.println("Reset: " + i + " " + counter);
                        row =0;
                        i=0;
                        counter =0;
                        for (int k=1;k<8;k++){
                            for (int l=0;l<8;l++) {
                                queens[k].setPosition((1+(int)(7.0 * Math.random())), ((int)(7.0 * Math.random()))); 
                            }
                            // queens[k].print();
                        }
                    }
                    
                    for (int col=0; col <= i; col++){
                        if (col == row) continue;
                        if (checkQueens(queens[row].attacks, queens[col].attacks)) {
                            conflict = true;
                            if((row == 0) && (col == 0)) queens[(1+(int)(7.0 * Math.random()))].nextPosition();
                            else if ((row == 0)) queens[col].nextPosition();
                            else  queens[row].nextPosition();
                        }
                    }
                    
                }   
                if (!conflict) {
                    // System.out.println("No collisions, leaving queen in array pos " + i + " at " + queens[i].row + ", " + queens[i].col);
                    break;
                }
            }
        }
        int retArray[] = new int[8];
        for (Queen queen : queens) {
            if (queen.row == 0) retArray[0] = queen.col;
            if (queen.row == 1) retArray[1] = queen.col;
            if (queen.row == 2) retArray[2] = queen.col;
            if (queen.row == 3) retArray[3] = queen.col;
            if (queen.row == 4) retArray[4] = queen.col;
            if (queen.row == 5) retArray[5] = queen.col;
            if (queen.row == 6) retArray[6] = queen.col;
            if (queen.row == 7) retArray[7] = queen.col;
        }
        Solution retValue = new Solution(retArray);
        return retValue;
    }

    public static boolean checkQueens(char [][] a, char[][] b){
        boolean attacks = false;
        for (int row=0; row < 8; row++) {
                    
            for (int col=0; col<8; col++){
                if ((a[row][col] == 'X') && (b[row][col] == 'D')) {
                    attacks = true;
                    // System.out.println("Conflict");
                }
                if ((a[row][col] == 'D') && (b[row][col] == 'X')) {
                    attacks = true;
                    // System.out.println("Conflict");
                }
                if ((a[row][col] == 'D') && (b[row][col] == 'D')) {
                    attacks = true;
                    // System.out.println("Conflict");
                }
            }
        }
        return attacks;
    }

    public static void printAllSolutions(Queen [] q){
        char board[][] = new char[8][8];

        for (Queen queen : q) {
            System.out.println("hei");
            for (int i=0; i<8; i++)
                for (int j=0;j<8;j++) 
                {
                    if (queen.attacks[i][j] == 'D') board[i][j] = 'D';
                }
        }
        drawBoard(board);
    }
    public static void printAllQueens(Queen [] q){
        char board[][] = new char[8][8];

        for (Queen queen : q) {
            for (int i=0; i<8; i++)
                for (int j=0;j<8;j++) 
                {
                    if (queen.attacks[i][j] == 'D') board[i][j] = 'D';
                }
        }
        drawBoard(board);
    }

    public static void drawBoard(char b[][]) {
        char board[][] = new char[8][8];
        boolean whiteSquare = false;
        
        for (int row=0; row < 8; row++) {

            for (int col=0; col<8; col++){
                
                if (whiteSquare) board[row][col] = '_';
                else board[row][col] = '#';
                whiteSquare = !whiteSquare;
                if (b[row][col] == 'D'){
                    board[row][col] = 'D';
                    //System.out.println("True");
                }
            }
        }
        System.out.println(" ABCDEFGH");
        // board = b;
        for (int row=7; row >= 0; row--) {
            System.out.print(row+1);
            
            for (int col=0; col<8; col++){
                System.out.print(board[row][col]);
                
            }
            System.out.print(row+1);
            System.out.println("");

        }
        System.out.println(" ABCDEFGH");

    }
        
    public static void drawBoard() {
        char board[][] = new char[8][8];
        boolean whiteSquare = false;

        for (int i=0; i<8; i++) {

            for (int j=0; j<8; j++){
                if (whiteSquare) board[i][j] = '_';
                else board[i][j] = '#';
                whiteSquare = !whiteSquare;
            }
        }
        System.out.println(" ABCDEFGH");

        for (int i=0; i<8; i++) {
            System.out.print(8-i);
            
            for (int j=0; j<8; j++){
                System.out.print(board[i][j]);
                
            }
            System.out.print(8-i);
            System.out.println("");

        }
        System.out.println(" ABCDEFGH");
    }
}