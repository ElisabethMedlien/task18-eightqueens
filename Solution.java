public class Solution {

    
    public Solution (){
        for (int i=0;i<8;i++)this.squares[i] = 9;
    }
    public Solution (int[] s){
        for (int i=0;i<8;i++)this.squares[i] = s[i];
    }

    public void print(){
        char board[][] = new char[8][8];
        System.out.println("Original:");
        for (int i=0; i<8; i++)
            for (int j=0;j<8;j++) 
            {
                if (this.squares[i] == j) board[i][j] = 'D';
            }
        drawBoard(board);


        char boardFlippedX[][] =  new char[8][8]; //board;

        System.out.println("Flipped X:");
        for (int i=0; i<8; i++)
            for (int j=0;j<8;j++) 
            {
                 boardFlippedX[i][j] = board[i][7-j];
            }
        drawBoard(boardFlippedX);

        char boardFlippedY[][] =  new char[8][8]; //board;

        System.out.println("Flipped Y:");
        for (int i=0; i<8; i++)
            for (int j=0;j<8;j++) 
            {
                 boardFlippedY[i][j] = board[7-i][j];
            }
        drawBoard(boardFlippedY);

        char boardFlippedXY[][] =  new char[8][8]; //board;

        System.out.println("Flipped XY:");
        for (int i=0; i<8; i++)
            for (int j=0;j<8;j++) 
            {
                 boardFlippedXY[i][j] = board[7-i][7-j];
            }
        drawBoard(boardFlippedXY);
    }



    public static void drawBoard(char b[][]) {
        char board[][] = new char[8][8];
        boolean whiteSquare = false;
        
        for (int row=0; row < 8; row++) {

            for (int col=0; col<8; col++){
                
                if (whiteSquare) board[row][col] = '_';
                else board[row][col] = '#';
                whiteSquare = !whiteSquare;
                if (b[row][col] == 'D'){
                    board[row][col] = 'D';
                }
            }
        }
        System.out.println(" ABCDEFGH");
        // board = b;
        for (int row=7; row >= 0; row--) {
            System.out.print(row+1);
            
            for (int col=0; col<8; col++){
                System.out.print(board[row][col]);
                
            }
            System.out.print(row+1);
            System.out.println("");

        }
        System.out.println(" ABCDEFGH");
        System.out.println("");
    }
    public int squares[] = new int[8];
}